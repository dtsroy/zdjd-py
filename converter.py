import base64

b64 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/='

leftEye = ['o', '0', 'O', 'Ö']
mouth = ['w', 'v', '\.', '_']
rightEye = ['o', '0', 'O', 'Ö']
table = []

separator = ' '

def makeTable():
    for i in leftEye:
        for j in mouth:
            for k in rightEye:
                table.append(f"{i}{j}{k}")

makeTable()

def human2zdjd(t:str):
    arr = [table[b64.index(i)] for i in base64.b64encode(t.encode("utf-8")).decode("utf-8")]
    return separator.join(arr)

def zdjd2human(t):
    ret = ""
    for i in t.split(separator):
        if not i:
            continue
        n = table.index(i)
        if n < 0:
            print("error.")
        ret += b64[n]
    return base64.b64decode(ret.encode("utf-8")).decode("utf-8")

def isZdjd(t):
    try:
        zdjd2human(t)
        return True
    except Exception as e:
        return False

